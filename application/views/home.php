
    <body>
        <div class="container">
            <ul class="nav nav-pills nav-fill sticky-top bg-warning" id="book"> 
                <li class="nav-item">
                    <p>Staycool <span id="spin"></span></p>
                </li>
                    <a class="nav-item nav-link active" href="<?php echo site_url('Login');?>">จองห้องพัก Click</a>
            </ul>
            <div class="row" id="Picture">
                <div class="col-sm-3 text-center">
                    <br><br>
                    <h1 class="text-warning fade-in-text"><b><u> Staycool </u></b></h1><br><br>
                    <p class="fade-in-text2"> "บ้านพักสัตว์เลี้ยง </p>
                    <p class="fade-in-text2"> สไตล์รีสอร์ทบ้านสวน </p>
                    <p class="fade-in-text3">  บริเวณกว้างขวาง </p>
                    <p class="fade-in-text3"> แยกสัดส่วนเป็นห้องๆ </p>
                    <p class="fade-in-text3"> บรรยากาศร่มรื่น"</p><br><br>
                </div>
                <div class="col-sm-9">
                    <div id="slider">  
                    <div class="slides">  
                        <img src="<?php echo base_url('img/slide/sd/4.png');?>" width="100%" />
                    </div>
                    <div class="slides">  
                        <img src="<?php echo base_url('img/slide/sd/2.png');?>" width="100%" />
                    </div>
                    <div class="slides">  
                        <img src="<?php echo base_url('img/slide/sd/3.png');?>" width="100%" />
                    </div>  
                    <div class="slides">  
                        <img src="<?php echo base_url('img/slide/sd/5.png');?>" width="100%" />
                    </div>
                    <div class="slides">  
                        <img src="<?php echo base_url('img/slide/sd/6.png');?>" width="100%" />
                    </div>
                        <div id="dot"><span class="dot"></span><span class="dot"></span><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" id="txt01" style="text-align:center">
                  <h1 class="cover-heading text-warning fade-in-text" ><b><u> Staycool </u></b></h1><br>
                  <p class="fade-in-text"><q>Price List</q></p><br>
              </div>
            
            <div class="columns">
                <ul class="price">
                    <li class="header">ห้องพัดลม</li>
                    <li class="grey text-danger">สุนัขขนาดเล็ก </li>
                    <li class="grey text-danger">(น้ำหนักไม่เกิน 15 กิโลกรัม)</li>
                    <li class="grey">350 ฿/ คืน</li>
                    <li class="grey">ส่วนลดลูกค้าใหม่</li>
                    <li class="grey">50 บาท</li>
                    <li class="grey"><a href="<?php echo site_url('Login');?>" class="button bg-primary rounded-pill">Booking</a></li>
                </ul>
              </div>
              <div class="columns">
                  <ul class="price">
                      <li class="header" style="background-color:#04AA6D">ห้องแอร์</li>
                      <li class="red text-danger">สุนัขขนาดเล็ก </li>
                      <li class="red text-danger">(น้ำหนักไม่เกิน 15 กิโลกรัม)</li>
                      <li class="red">450 ฿ / คืน</li>
                      <li class="red" >ส่วนลดลูกค้าใหม่</li>
                      <li class="red">50 บาท</li> 
                      <li class="red"><a href="<?php echo site_url('Login');?>" class="button bg-primary rounded-pill">Booking</a></li>
                  </ul>
              </div>
              <div class="columns">
                  <ul class="price">
                      <li class="header">ห้องพัดลม</li>
                      <li class="green text-danger">สุนัขขนาดใหญ่ </li>
                      <li class="green text-danger">(น้ำหนักเกิน 15 กิโลกรัม)</li>
                      <li class="green">800 ฿/ คืน</li>
                      <li class="green">ส่วนลดลูกค้าใหม่</li>
                      <li class="green">50 บาท</li>
                      <li class="green"><a href="<?php echo site_url('Login');?>" class="button bg-primary rounded-pill">Booking</a></li>
                  </ul>
              </div>
              <div class="columns">
                  <ul class="price">
                      <li class="header" style="background-color:#04AA6D">ห้องแอร์</li>
                      <li class="yellow text-danger">สุนัขขนาดใหญ่ </li>
                    <li class="yellow text-danger">(น้ำหนักเกิน 15 กิโลกรัม)</li>
                      <li class="yellow">1,200 ฿ / คืน</li>
                      <li class="yellow">ส่วนลดลูกค้าใหม่</li>
                      <li class="yellow">50 บาท</li>
                      <li class="yellow"><a href="<?php echo site_url('Login');?>" class="button bg-primary rounded-pill">Booking</a></li>
                  </ul>
              </div>
              <div class="col-md-12" id="txt01" style="text-align:center">
                  <h1 class="cover-heading text-warning fade-in-text" ><b><u> Staycool </u></b></h1><br>
                  <p class="fade-in-text"><q>อยู่บ้านตัวเดียวมันเหงา ... มาอยู่กับเราสิจ๊ะ </q></p><br>
              </div>
              <div class="row9">
                <div class="column9">
                  <div class="card">
                    <img src="<?php echo base_url('img/pd.jpg');?>" alt="Pidtong" style="width:100%">
                      <div class="container9">
                        <h2>ปิดทอง</h2>
                          <p class="title9">CEO &amp; Founder</p>
                          <p>หนูเป็น บีเกิ้ล บ้าพลัง ขี้งอน ชอบประจบ </p>
                          <p>มาเที่ยวหาหนูได้นะคะ</p>
                      </div>
                    </div>
                  </div>

                <div class="column9">
                  <div class="card">
                    <img src="<?php echo base_url('img/slide/93355.jpg');?>" alt="Mike" style="width:100%">
                      <div class="container9">
                        <h2>อองฟอง</h2>
                        <p class="title9">Art Director</p>
                        <p>หนูคือ ปอมเปอร์เรเนี่ยน ขี้กลัว แต่ใจดี</p>
                        <p>พาพี่ๆมาเล่นกับหนูได้นะคะ</p>
                      </div>
                  </div>
                </div>

                <div class="column9">
                  <div class="card">
                    <img src="<?php echo base_url('img/slide/90270.jpg');?>" alt="John" style="width:100%">
                      <div class="container9">
                        <h2>แม่มณี</h2>
                        <p class="title9">Designer</p>
                        <p>หนูเป็นกระต่าย ชื่อ แม่มณี มีชาวแก็งค์อีกหลายตัว </p>
                        <p>มาเล่นกับพวกเราได้นะจ๊ะ</p>
                      </div>
                    </div>
                  </div>
                </div>
                
              <div class="col-md-12 text-center" id="txt01">
                 
                  <h1 class="cover-heading text-warning fade-in-text" ><b><u> Our Picture </u></b></h1><br>
                  <p class="fade-in-text"><q>บ้านสวน ร่มรื่น กว้างขวาง</q></p><br>
              </div>
              <div class="row2"> 
                <div class="column3">
                    <img src="<?php echo base_url('img/slide/35711.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/90261.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/90265.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/90270.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/90276.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/90305.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/92920.jpg');?>" style="width:100%">
                </div>
                <div class="column3">
                    <img src="<?php echo base_url('img/slide/92931.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93309.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93323.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93344.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93355.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93376.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/sd/3.png');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/sd/2.png');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/sd/4.png');?>" style="width:100%">
                </div>  
                <div class="column3">
                    <img src="<?php echo base_url('img/slide/93460.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93475.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93477.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93485.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93501.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93541.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/93553.jpg');?>" style="width:100%">
                </div>
                <div class="column3">
                    <img src="<?php echo base_url('img/slide/991.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/S__87965720.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/S__87965722.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/S__87965723.jpg');?>" style="width:100%">
                    <img src="<?php echo base_url('img/slide/S__87965724.jpg');?>" style="width:100%">
                </div>
            </div>
            <div class="col-md-12" id="txt01" style="text-align:center">
                  <h1 class="cover-heading text-warning fade-in-text" ><b><u> Contact us </u></b></h1><br>
                  <p class="fade-in-text"><q>Staycool</q></p><br>
              </div>
            
              <div class="row" id="maps">
                  
              <div class="col-md-4 text-center">
              <h1 class="text-info"><i class="fas fa-home"></i></h1><br>
                <h2 class="text-danger">Staycool </h2>
                <br>
                <h5>
                50/3 moo 7 <br>
                Bangmaenang <br>
                Bangyai <br>
                Nontaburi <br>
                11140 <br><br></h5>
                <h4 class=""><i class="fas fa-phone text-primary"></i> 085-9965443</h4><br>
                <h4 class=""><i class="fab fa-line text-success"></i> @Staycool239</h4>
                
              </div>  
                  <div class="col-md-4 text-center">
                    <h1 class="cover-heading text-primary" ><i class="fab fa-facebook"></i> <b><u> Facebook </u></b></h1>
                    
                    <div class="fb-page" data-href="https://www.facebook.com/staycoollekgarden/" data-tabs="timeline" data-width="" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                  </div>
                  <div class="col-md-4 text-center" >
                    <h1 class="cover-heading text-danger" ><i class="fas fa-map"></i><b><u> Google Maps </u></b></h1>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3873.9144860369756!2d100.36796201490219!3d13.844171398875973!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e28f88e0f35377%3A0x2d90e7ecb2c20601!2sStaycool!5e0!3m2!1sth!2sth!4v1643185720309!5m2!1sth!2sth" width="400" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
                  </div>
                  </div>
              </div>

                          
            <div class="col-md-12" id="footer">
            <div class="row">
                    <div class="col-xl-6 col-lg-6 text-center text-lg-left">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2022, All Right Reserved <a href="#">De'Trin</a></p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 d-none d-lg-block text-right">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#price">Price</a></li>
                                <li><a href="#txt01">Picture</a></li>
                                <li><a href="#">Policy</a></li>
                                <li><a href="#maps">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
