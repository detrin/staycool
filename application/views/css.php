<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.3.1/css/all.css'>
<style>
@import url('https://fonts.googleapis.com/css2?family=Itim&display=swap');
</style>
<style>
  
*{
    padding: 0;
    margin:0;
    box-sizing: border-box;
    font-family: 'Itim', cursive;
}
p {
  font-size:1.1em;
}
nav {
  font-size:1.5em;
}
#slider{
  margin:0 auto;
  width:100%;
  overflow:hidden;
}
#picture{
  margin:20px;
}
#book{
  padding:5px;
  font-size: 1.3em;
}
.slides{
  overflow:hidden;
  animation-name:fade;
  animation-duration:3s;
  display:none;
}

#dot{
  margin:0 auto;
  text-align:center;
}
.dot{
  display:inline-block;
  border-radius:50%;
  background:#d3d3d3;
  padding:8px;
  margin:10px 5px;
}

.active{
  background:black;
}

@media (max-width:567px){
  #slider{
    width:100%;
  }
}

#heading{
  display:block;
  text-align:center;
  font-size:2em;
  margin:10px 0px;

}
.fade-in-text {
  display: inline-block;
  font-family: Arial, Helvetica, sans-serif;
  color: black;
  animation: fadeIn linear 3s;
  -webkit-animation: fadeIn linear 3s;
  -moz-animation: fadeIn linear 3s;
  -o-animation: fadeIn linear 3s;
  -ms-animation: fadeIn linear 3s;
}
.fade-in-text2 {
  display: inline-block;
  font-family: Arial, Helvetica, sans-serif;
  color: black;
  animation: fadeIn linear 4s;
  -webkit-animation: fadeIn linear 4s;
  -moz-animation: fadeIn linear 4s;
  -o-animation: fadeIn linear 4s;
  -ms-animation: fadeIn linear 4s;
}
.fade-in-text3 {
  display: inline-block;
  font-family: Arial, Helvetica, sans-serif;
  color: black;
  animation: fadeIn linear 5s;
  -webkit-animation: fadeIn linear 5s;
  -moz-animation: fadeIn linear 5s;
  -o-animation: fadeIn linear 5s;
  -ms-animation: fadeIn linear 5s;
}

@keyframes fadeIn {
  0% {opacity:0;}
  100% {opacity:1;}
}

@-moz-keyframes fadeIn {
  0% {opacity:0;}
  100% {opacity:1;}
}

@-webkit-keyframes fadeIn {
  0% {opacity:0;}
  100% {opacity:1;}
}

@-o-keyframes fadeIn {
  0% {opacity:0;}
  100% {opacity:1;}
}

@-ms-keyframes fadeIn {
  0% {opacity:0;}
  100% {opacity:1;}
}

#spin {
  color:red;
  padding-top:2px;
  align-items:center;
  font-size:15px;
}
#spin:after {
  content:"";
  animation: spin 15s linear infinite;
}
@keyframes spin {
  0% { content:"บ้านพัก";}
  10% { content:"บรรยากาศ";}
  20% { content:"บ้านสวน";}
  30% { content:"ดูแลตลอด";}
  40% { content:"24 Hr";}
  50% { content: "อบอุ่น"; }
  60% { content: "สถานที่"; }
  70% { content: "กว้างขวาง"; }
  80% { content: "ปลอดภัย"; }
  90% { content: "ร่มรื่น"; }
}

.columns {
  float: left;
  width: 25%;
  padding: 8px;
}

.columns_2 {
  float: left;
  width: 50%;
  padding: 8px;
}

.price {
  list-style-type: none;
  border: 1px solid #eee;
  margin: 0;
  padding: 0;
  -webkit-transition: 0.3s;
  transition: 0.3s;
}

.price:hover {
  box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
}

.price .header {
  background-color: #FFCC00;
  color: white;
  font-size: 1em;
}

.price li {
  border-bottom: 1px solid #eee;
  padding: 20px;
  text-align: center;
  font-size: 20px;
}

.price .grey {
  background-color: #F5F5F5;
}
.price .red {
  background-color: #FAF0E6;
}
.price .green {
  background-color: #FFE4E1;
}
.price .yellow {
  background-color:#E6E6FA;
}
.button {
  background-color: #0000FF;
  border: none;
  color: white;
  padding: 10px 25px;
  text-align: center;
  text-decoration: none;
  font-size: 18px;
}

@media only screen and (max-width: 600px) {
  .columns {
    width: 100%;
  }
}

.fade-in-text {
  display: inline-block;
  font-family: Arial, Helvetica, sans-serif;
  color: black;
  animation: fadeIn linear 3s;
  -webkit-animation: fadeIn linear 3s;
  -moz-animation: fadeIn linear 3s;
  -o-animation: fadeIn linear 3s;
  -ms-animation: fadeIn linear 3s;
}
#txt01 {
  padding:20px;
}
#text5{
  margin-bottom:20px;
}
.row2 {
    display: -ms-flexbox; /* IE10 */
    display: flex;
    -ms-flex-wrap: wrap; /* IE10 */
    flex-wrap: wrap;
    padding: 0 4px;
  }

/* Create four equal columns that sits next to each other */
.column3 {
    -ms-flex: 25%; /* IE10 */
    flex: 25%;
    max-width: 25%;
    padding: 0 4px;
  }
  
  .column3 img {
    margin-top: 8px;
    vertical-align: middle;
    width: 100%;
  }
  
  /* Responsive layout - makes a two column-layout instead of four columns */
  @media screen and (max-width: 800px) {
    .column3 {
      -ms-flex: 50%;
      flex: 50%;
      max-width: 50%;
    }
}
  /* Responsive layout - makes the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
    .column3 {
        -ms-flex: 100%;
        flex: 100%;
        max-width: 100%;
    }
}

#maps {
  display:flex;
}
#footer {
  padding:10px;
}
.copyright-area{
  background: #202020;
  padding: 25px 0;
}
.copyright-text p {
  margin: 0;
  font-size: 14px;
  color: #878787;
}
.copyright-text p a{
  color: #ff5e14;
}
.footer-menu li {
  display: inline-block;
  margin-left: 20px;
}
.footer-menu li:hover a{
  color: #ff5e14;
}
.footer-menu li a {
  font-size: 14px;
  color: #878787;
}
/* Three columns side by side */
.column9 {
  float: left;
  width: 33.3%;
  margin-bottom: 16px;
  padding: 0 8px;
}

/* Display the columns below each other instead of side by side on small screens */
@media screen and (max-width: 650px) {
  .column9 {
    width: 100%;
    display: block;
  }
}

/* Add some shadows to create a card effect */
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

/* Some left and right padding inside the container */
.container9 {
  padding: 0 16px;
}

/* Clear floats */
.container9::after, .row::after {
  content: "";
  clear: both;
  display: table;
}

.title9 {
  color: grey;
}

.button9 {
  border: none;
  outline: 0;
  display: inline-block;
  padding: 8px;
  color: white;
  background-color: #000;
  text-align: center;
  cursor: pointer;
  width: 100%;
}

.button:hover {
  background-color: #555;
}
</style>